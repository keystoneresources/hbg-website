$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;

	if($('body.type-1').length){
    $('#home_slider .slider').cycle({
      speed: 3000,
      timeout: 8000,
      fx: 'fade',
      width: '100%',
      fit: 1,
      pager: '.pager',
      pagerTemplate: "<a href=# class=text-hide> {{slideNum}} </a>",
      slides: '> div'
    }); 
	    if($('.pager a').length === 1){
		    $('.pager a').hide();
	    } 
	}
	
	if($('body.type-4').length){
		$('.columned_list').columnizeList({columnAmount:2});  
	}	


	if($('body.type-6').length){
/*
		$('#garden_slider .slider').cycle({
      speed: 2000,
      timeout: 5500,
      fx: 'fade',
      width: '100%',
      fit: 1,
      prev: '.arrow.prev',
      next: '.arrow.next'
    });
*/
    
    $( '.cycle-slideshow' ).on( 'cycle-before', function( event, opts ) {
      $('.sliderContent').each(function(){
        $(this).hide();
      });
      $('.sliderContent.slide'+opts.slideid).show();
    });
    
	}

	if ($('body.type-9').length){
    $('#contact_form').validate();
  }//if
  
	if ($('body.type-10').length){
		Array.prototype.indexOf = function(obj, start) {
		 for (var i = (start || 0), j = this.length; i < j; i++) {
			 if (this[i] === obj) { return i; }
		 }
		 return -1;
		}
		var dates = [];
		var activeMonth = $('ul.datenav').attr("data-active-month");
		var activeYear = $('ul.datenav').attr("data-active-year");
		$(".datenav li").each(function(index, item){
				var m = $(this).attr('data-month'); 
				if(m == activeMonth){
					$(this).addClass('active'); 
				}
			if(dates.indexOf($(this).attr('data-year'))<0)
			{ 
				var d = $(this).attr('data-year'); 
				if(d == activeYear){
					var htmlinclude = '<li class="active">' + d + '</li>';
				}else{
					var htmlinclude = '<li>' + d + '</li>';
				}
				$(htmlinclude).insertBefore($(this));
				dates.push(d);
			}
		}); 
		
	}


  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//snapDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }


}); //End Document Ready